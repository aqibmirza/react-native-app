import React, { useRef, useState } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, View, TextInput, StatusBar, ImageBackground, Dimensions } from 'react-native';
import { Button, GradientButton, Input, Loading } from '#/components/_reusable';
// import { tl } from '#/utils/i18n';
import { colors, H0, P1, H5,P4 } from '#/designSystem';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { goToLogin } from '#/modules/Authentication/actions';

const { height, width } = Dimensions.get('window');

const SignupForm = props => {
  const [firstName, setFirstName] = useState(''); 
  const [middleName, setMiddleName] = useState(''); 
  const [lastName, setLastName] = useState(''); 
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [alternateMobileNumber, setAlternateMobileNumber] = useState('');
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
 

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const middleNameRef = useRef(null);
  const lastNameRef = useRef(null);
  const emailRef = useRef(null);
  const mobileNumberRef = useRef(null);
  const alternateMobileNumberRef = useRef(null);
  const userNameRef = useRef(null);
  const passwordRef = useRef(null);
  const confirmPasswordRef = useRef(null);
 
  const disabled = !email || !password

  const onSubmit = () => {
    if (disabled) return;
    
    setLoading(true);
    setError(null);
    // try {
    //   // await props.loginCprFlow(cpr);
    // } catch (e) {
    //   setError(e.message);
    // }
    setLoading(false);
  };
  const renderButton = () => {
    if (loading) return <Loading transparent />;
    return (
      <Button disabled={disabled} onPress={onSubmit} opacity={!disabled ? 1 : 0.5 } style={{marginTop:10}}>
        Next
      </Button>
    );
  };

  return (
      <KeyboardAwareScrollView contentContainerStyle={styles.screenContainerStyle}>
        <View>
          <Input
            onChangeText={(value)=> setFirstName(value)}
            value={firstName}
            onSubmitEditing={() => middleNameRef.current.focus()}
            error={error}
            keyboardType="default"
            placeholder="Enter the first name"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            autoCompleteType="name"
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setMiddleName(value)}
            value={middleName}
            onSubmitEditing={() => lastNameRef.current.focus()}
            error={error}
            keyboardType="default"
            placeholder="Enter the middle name or initial name"
            autoCompleteType="name"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={middleNameRef}
            
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setLastName(value)}
            value={lastName}
            onSubmitEditing={() => emailRef.current.focus()}
            error={error}
            keyboardType="default"
            placeholder="Enter the last name"
            autoCompleteType="name"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={lastNameRef}
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setEmail(value)}
            value={email}
            onSubmitEditing={() => mobileNumberRef.current.focus()}
            error={error}
            keyboardType="default"
            placeholder="Enter the email"
            autoCompleteType="email"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={emailRef}
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setMobileNumber(value)}
            value={mobileNumber}
            onSubmitEditing={() => alternateMobileNumberRef.current.focus()}
            error={error}
            keyboardType="phone-pad"
            placeholder="Enter the mobile no."
            autoCompleteType="tel"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={mobileNumberRef}
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setAlternateMobileNumber(value)}
            value={alternateMobileNumber}
            onSubmitEditing={() => userNameRef.current.focus()}
            error={error}
            keyboardType="phone-pad"
            placeholder="Enter the alternate no."
            autoCompleteType="tel"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={alternateMobileNumberRef}
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setUserName(value)}
            value={userName}
            onSubmitEditing={() => passwordRef.current.focus()}
            error={error}
            keyboardType="default"
            placeholder="Enter the username"
            autoCompleteType="username"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={userNameRef}
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setPassword(value)}
            value={password}
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
            error={error}
            keyboardType="default"
            placeholder="Enter the password"
            autoCompleteType="password"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={passwordRef}
          />
        </View>
        <View>
          <Input
            onChangeText={(value)=> setConfirmPassword(value)}
            value={confirmPassword}
            error={error}
            keyboardType="default"
            placeholder="Enter the confirm password"
            autoCompleteType="password"
            returnKeyType="next"
            styleInput={{color:colors.black}}
            ref={confirmPasswordRef}
          />
        </View>
       
       
        <View>{renderButton()}</View>
        <View style={{flexDirection:'row',justifyContent:'center'}}>
        <P4 style={styles.registerText}>Don’t have a account? </P4><H5 style={{marginVertical:10,height:20}} color={colors.blue} onPress={() => props.goToLogin()}>Login</H5>
        </View>
      </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  screenContainerStyle:{
    flexGrow:1,
    justifyContent:"space-between",
    backgroundColor:colors.white,
    paddingHorizontal:38
  },
  forgotPassword:{
    textAlign:'right',
    marginVertical:10
  },
  registerText:{
    marginTop:10,
    marginBottom:60
  },
  logoStyle:{
    justifyContent:'center',
    alignItems:'center',
    marginBottom:30
  }
});

const ConnectedSignupForm = connect(null, {
goToLogin
})(SignupForm);

export { ConnectedSignupForm as SignupForm };
