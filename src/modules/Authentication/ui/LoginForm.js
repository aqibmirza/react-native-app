import React, { useRef, useState } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, View, TextInput, StatusBar, ImageBackground, Dimensions } from 'react-native';
import { Button, GradientButton, Input, Loading } from '#/components/_reusable';
// import { tl } from '#/utils/i18n';
import { colors, H0, P1, H5,P4 } from '#/designSystem';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Envelope from '#/resources/img/svg/Envelope';
import Lock from '#/resources/img/svg/Lock';
import { goToSignup,goToForgotPassword } from '#/modules/Authentication/actions';

const { height, width } = Dimensions.get('window');

const LoginForm = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const passwordRef = useRef(null);
  const disabled = !email || !password

  const onSubmit = () => {
    if (disabled) return;
    

  };
  const renderButton = () => {
    if (loading) return <Loading transparent />;
    return (
      <Button disabled={disabled} onPress={onSubmit} opacity={!disabled ? 1 : 0.5 } style={{marginTop:10}}>
        LOGIN
      </Button>
    );
  };

  return (
    <ImageBackground source={{ uri: "loginbg" }} style={styles.image}>
      <KeyboardAwareScrollView contentContainerStyle={styles.screenContainerStyle}>
        <View style={styles.loginBox}>
        <H0 centered>Welcome back</H0>
        <P1 centered style={{ marginBottom: 30 }}>Login to your account</P1>
        <View>
          <Input
            onChangeText={(value)=> setEmail(value)}
            value={email}
            onSubmitEditing={() => passwordRef.current.focus()}
            error={error}
            keyboardType="email-address"
            placeholder="Your Email"
            autoCompleteType="email"
            returnKeyType="next"
            icon={<Envelope/>}
            styleInput={{color:colors.black}}
          />
        </View>
        <View>
          <Input
            onChangeText={(value) => setPassword(value)}
            value={password}
            onSubmitEditing={onSubmit}
            error={error}
            keyboardType="default"
            placeholder="Password"
            autoCompleteType="password"
            returnKeyType="done"
            icon={<Lock />}
            ref={passwordRef}
            styleInput={{color:colors.black}}
          />
        </View>
        <H5 style={styles.forgotPassword} color={colors.blue} onPress={() => props.goToForgotPassword()}>Forgot Password?</H5>
        <View>{renderButton()}</View>
        <View style={{flexDirection:'row',justifyContent:'center'}}>
        <P4 style={styles.registerText}>Don’t have a account? </P4><H5 style={{marginVertical:10,height:20}} color={colors.blue} onPress={() => props.goToSignup()}>Register</H5>
        </View>
        </View>
      </KeyboardAwareScrollView>

    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  screenContainerStyle:{
    flexGrow:1,
    justifyContent:"space-between"
  },
  image: {
    flex: 1,
    resizeMode: "cover"
  },
  loginBox:{
    backgroundColor:colors.white,
    paddingTop:20,
    paddingHorizontal:38,
    borderTopLeftRadius:26,
    borderTopRightRadius:26,
    paddingBottom:60
  },
  forgotPassword:{
    textAlign:'right',
    marginVertical:10
  },
  registerText:{
    marginTop:10,
    marginBottom:60
  }
});

const ConnectedLoginForm = connect(null, {
  goToSignup,
  goToForgotPassword
})(LoginForm);

export { ConnectedLoginForm as LoginForm };
