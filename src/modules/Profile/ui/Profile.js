import React, { useRef, useState } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, View, TextInput, StatusBar, ImageBackground, Dimensions, Text } from 'react-native';
import { Button, GradientButton, Input, Loading, StatusBarColor, DrawerMenu, AdvancedHeader } from '#/components/_reusable';
// import { tl } from '#/utils/i18n';
import { colors, H0, P1, H5, P4, H2 } from '#/designSystem';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { goToLogin } from '#/modules/Authentication/actions';
import { Drawer, Root } from 'native-base';
import SideMenu from '#/components/SideMenu';
import Hamburger from '#/resources/img/svg/Hamburger';

const { height, width } = Dimensions.get('window');

const Profile = props => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const drawerRef = useRef(null);

  const disabled = !email
  closeDrawer = () => {
    drawerRef.current._root.close()
  }
  openDrawer = () => {
    drawerRef.current._root.open()
  }
  const onSubmit = () => {
    if (disabled) return;

    setLoading(true);
    setError(null);
    // try {
    //   // await props.loginCprFlow(cpr);
    // } catch (e) {
    //   setError(e.message);
    // }
    setLoading(false);
  };

  const ProfileImage = (props) => {
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 35 }}>
        <H2 style={{ marginBottom: 8, fontWeight: '500' }} color={colors.white}>Total Earning</H2>
        <H0 color={colors.white} style={{ fontWeight: '600' }}>$ 1000</H0>
      </View>
    );
  }

  return (
    <DrawerMenu
      ref={drawerRef}
      close={closeDrawer} >
      <AdvancedHeader 
        minHeight={197} 
        leftIcon={<Hamburger />} 
        title="My Earning" 
        component={<ProfileImage />}
        leftOnPress={() => openDrawer()}
      />
      <KeyboardAwareScrollView>
      </KeyboardAwareScrollView>
    </DrawerMenu>
  );
};

const styles = StyleSheet.create({
  screenContainerStyle: {
    flexGrow: 1,
    justifyContent: "space-between"
  },
  image: {
    flex: 1,
    resizeMode: "cover"
  },
  loginBox: {
    backgroundColor: colors.white,
    paddingTop: 20,
    paddingHorizontal: 38,
    borderTopLeftRadius: 26,
    borderTopRightRadius: 26,
    paddingBottom: 60
  },
  forgotPassword: {
    textAlign: 'right',
    marginVertical: 10
  },
  registerText: {
    marginTop: 10,
    marginBottom: 60
  }
});

const ConnectedProfile = connect(null, {
  goToLogin
})(Profile);

export { ConnectedProfile as Profile };
