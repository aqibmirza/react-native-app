import uuid from 'uuid/v4';
import { SERVER_URL } from 'react-native-dotenv';
import { batch } from 'react-redux';
import { saveToCache, fetchFromCache } from '#/modules/Cache/actions';
import { refreshAccessToken, setRefreshPromise } from '#/modules/Authentication/actions';
import { accessTokenSelector, refreshPromiseSelector } from '#/modules/Authentication/reducer';

const API = '[API]';
export const API_REQUEST = `${API} Fetch from server`;
export const API_REQUEST_SUCCESS = `${API_REQUEST} Success`;
export const API_REQUEST_ERROR = `${API_REQUEST} Error`;

const apiRequest = request => ({
  type: API_REQUEST,
  payload: { request }
});
const apiRequestSuccess = (request, result) => ({
  type: API_REQUEST_SUCCESS,
  payload: { request, result }
});
const apiRequestError = (request, error) => ({
  type: API_REQUEST_ERROR,
  payload: { request, error }
});

/*
 *If there is a valid response cached, it is dispatched with the success handler.
 *If there is no valid response, it makes an API call to server.
 *If onError handler is not provided, the error is thrown.
 *Retry is a flag used to prevent infinite refreshAccessToken loops.
 */
export const apiCall = ({ method, params = [], onSuccess, onError, retry = true }) => async (
  dispatch,
  getState
) => {
  const request = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ jsonrpc: '2.0', id: uuid.v4(), method, params }),
    credentials: 'include'
  };

  //Avoid adding null Authorization header
  const accessToken = accessTokenSelector(getState());
  if (accessToken) request.headers.Authorization = `Bearer ${accessToken}`;

  try {
    const validCache = await dispatch(fetchFromCache(method, params, onSuccess));
    if (validCache) return;

    dispatch(apiRequest(request));
    const { result, error } = await fetch(SERVER_URL, request).then(response => response.json());

    /*If the error was a SecurityException, it is probably becuase the access token expired.
     *So try to refresh it once, and trigger again the same API call.
     *If it fails twice, then it throws the exception.
     */
    if (error) {
      const exception = new ServerInternalError(error);
      
      throw exception;
    }

    //Success handling
    batch(() => {
      dispatch(apiRequestSuccess(request, result));
      if (onSuccess) dispatch(onSuccess(result));
      dispatch(saveToCache(method, params, result));
    });
  } catch (error) {
    //Error handling
    batch(() => {
      dispatch(apiRequestError(request, error));
      if (onError) {
        dispatch(onError(error));
      } else {
        throw error;
      }
    });
  }
  return;
};

function ServerInternalError(error) {
  this.error = ((error || {}).data || {}).messageBundleCode || ((error || {}).data || {}).exception;
  this.message = this.error;
  this.toString = () => `${this.message}\n ${JSON.stringify(this.fullError)}`;
  this.fullError = error;
}
