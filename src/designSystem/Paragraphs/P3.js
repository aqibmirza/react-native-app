import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '../_styles/colors';

const P3 = props => (
  <Text
    onPress={props.onPress}
    style={{
      fontSize: 14,
      color: props.color,
      opacity: props.opacity,
      textDecorationLine: props.underlined ? 'underline' : 'none',
      textAlign: props.centered ? 'center' : 'left',
      ...props.style,
    }}>
    {props.children}
  </Text>
);

P3.propTypes = {
  children: PropTypes.string,
  onPress: PropTypes.func,
  color: PropTypes.string,
  opacity: PropTypes.number,
  underlined: PropTypes.bool,
  centered: PropTypes.bool,
};

P3.defaultProps = {
  children: '',
  onPress: null,
  color: colors.charcol,
  opacity: 0.5,
  underlined: false,
  centered: false,
};

export {P3};
