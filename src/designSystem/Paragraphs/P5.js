import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '../_styles/colors';

const P5 = props => (
  <Text
    style={{
      fontSize: 10,
      color: props.color,
      opacity: props.opacity,
      textDecorationLine: props.underlined ? 'underline' : 'none',
      textAlign: props.centered ? 'center' : 'left',
      ...props.style,
    }}>
    {props.children}
  </Text>
);

P5.propTypes = {
  children: PropTypes.string,
  color: PropTypes.string,
  opacity: PropTypes.number,
  underlined: PropTypes.bool,
  centered: PropTypes.bool,
};

P5.defaultProps = {
  children: '',
  color: colors.charcol,
  opacity: 0.5,
  underlined: false,
  centered: false,
};

export {P5};
