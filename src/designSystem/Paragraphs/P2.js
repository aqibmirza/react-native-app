import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '../_styles/colors';

const P2 = props => (
  <Text
    style={{
      fontSize: 16,
      color: props.color,
      opacity: props.opacity,
      textDecorationLine: props.underlined ? 'underline' : 'none',
      textAlign: props.centered ? 'center' : 'left',
      ...props.style,
    }}>
    {props.children}
  </Text>
);

P2.propTypes = {
  children: PropTypes.string,
  color: PropTypes.string,
  opacity: PropTypes.number,
  underlined: PropTypes.bool,
  centered: PropTypes.bool,
};

P2.defaultProps = {
  children: '',
  color: colors.charcol,
  opacity: 1,
  underlined: false,
  centered: false,
};

export {P2};
