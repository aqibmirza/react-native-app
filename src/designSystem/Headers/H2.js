import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '../_styles/colors';

const H2 = props => (
  <Text
    style={{
      fontSize: 24,
      color: props.color,
      opacity: props.opacity,
      textDecorationLine: props.underlined ? 'underline' : 'none',
      textAlign: props.centered ? 'center' : 'left',
      ...props.style,
    }}>
    {props.children}
  </Text>
);

H2.propTypes = {
  children: PropTypes.string,
  color: PropTypes.string,
  opacity: PropTypes.number,
  underlined: PropTypes.bool,
  centered: PropTypes.bool,
};

H2.defaultProps = {
  children: '',
  color: colors.charcol,
  opacity: 1,
  underlined: false,
  centered: false,
};

export {H2};
