import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '#/designSystem/_styles/colors';

const PageTitle = props => (
  <Text
    style={{
      fontSize: 20,
      color: props.color,
      textAlign: props.centered ? 'center' : 'left',
      fontWeight: props.fontWeight,
      ...props.style,
    }}>
    {props.children}
  </Text>
);

PageTitle.propTypes = {
  children: PropTypes.string,
  color: PropTypes.string,
  underlined: PropTypes.bool,
  centered: PropTypes.bool,
  fontWeight: PropTypes.string,
};

PageTitle.defaultProps = {
  children: '',
  color: colors.white,
  underlined: false,
  centered: false,
  fontWeight: '500',
};

export {PageTitle};
