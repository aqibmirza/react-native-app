import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '../_styles/colors';

const H6 = props => (
  <Text
    onPress={props.onPress}
    style={{
      fontSize: 10,
      letterSpacing: 1,
      color: props.color,
      opacity: props.opacity,
      textDecorationLine: props.underlined ? 'underline' : 'none',
      textAlign: props.centered ? 'center' : 'left',
      ...props.style,
    }}>
    {props.children}
  </Text>
);

H6.propTypes = {
  children: PropTypes.string,
  onPress: PropTypes.func,
  color: PropTypes.string,
  opacity: PropTypes.number,
  underlined: PropTypes.bool,
  centered: PropTypes.bool,
};

H6.defaultProps = {
  children: '',
  color: colors.charcol,
  opacity: 1,
  underlined: false,
  centered: false,
  onPress: null,
};

export {H6};
