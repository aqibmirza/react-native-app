import { combineReducers } from 'redux';
import storage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';


import ScreenVisitedReducer from './ScreenVisitedReducer';


const screenVisitedPersistConfig = {
  key: 'screenVisited',
  storage,
  whitelist: [''],
  stateReconciler: autoMergeLevel2
};

const appReducer = combineReducers({
  screenVisited: persistReducer(screenVisitedPersistConfig, ScreenVisitedReducer),
});

export default appReducer;
