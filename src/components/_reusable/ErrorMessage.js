import React from 'react';
import { StyleSheet } from 'react-native';
import { P2, colors } from '../../designSystem';

const ErrorMessage = ({ description }) => {
  const { errorTextStyle } = styles;
  if (description) {
    return <P2 style={errorTextStyle}>{description}</P2>;
  }
  return null;
};

const styles = StyleSheet.create({
  errorTextStyle: {
    color: colors.red,
    marginTop: 4
  }
});

export { ErrorMessage };
