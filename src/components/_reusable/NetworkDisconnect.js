import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Button} from './Button';
// import { tl } from '../../utils/i18n';
import {H2, P1, colors} from '../../designSystem';

const NetworkDisconnect = ({onPress}) => (
  <View style={styles.containerStyle}>
    <View style={{marginTop: 60}}>
      <H2 centered>Disconnected</H2>
      <P1 centered>InternetDisconnected</P1>
    </View>
    <Button onPress={onPress} style={styles.buttonStyle}>
      Button
    </Button>
  </View>
);

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: colors.greyDark,
  },
  buttonStyle: {
    width: '100%',
  },
});

export {NetworkDisconnect};
