import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
// import { Touchable } from './Touchable';
import { Drawer } from 'native-base';
import SideMenu from '#/components/SideMenu';


const DrawerMenu = React.forwardRef(({ close, children }, ref) => (
  <Drawer
    width={100}
    ref={ref}
    content={<SideMenu close={() => close()}/>}
    type="displace"
    openDrawerOffset={0.25}
    onClose={() => close()} >

    {children}

  </Drawer>
));

const styles = StyleSheet.create({
  menuStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 15
  },
  buttonStyle: {
    width: 46,
    height: 46,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export { DrawerMenu };
