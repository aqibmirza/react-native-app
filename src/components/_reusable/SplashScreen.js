import React from 'react';
import {View, ImageBackground, StyleSheet} from 'react-native';

const SplashScreen = () => (
  <View style={styles.container}>
    <ImageBackground
      source={{uri: 'https://reactjs.org/logo-og.png'}}
      style={styles.splashImage}
    />
  </View>
);
export {SplashScreen};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  splashImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
