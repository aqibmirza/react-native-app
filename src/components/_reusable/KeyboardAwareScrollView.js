import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  ScrollView,
  Platform,
  Keyboard,
  Dimensions,
  TextInput,
  UIManager,
  Easing
} from 'react-native';
import Animated from 'react-native-reanimated';

const { State: TextInputState } = TextInput;
const { height: windowHeight } = Dimensions.get('window');

const KeyboardAwareScrollView = props => {
  const [shift] = useState(new Animated.Value(0));

  useEffect(() => {
    const keyboardDidShowSub = Keyboard.addListener('keyboardWillShow', handleKeyboardDidShow);
    const keyboardDidHideSub = Keyboard.addListener('keyboardWillHide', handleKeyboardDidHide);

    return () => {
      keyboardDidShowSub.remove();
      keyboardDidHideSub.remove();
    };
  }, []);

  const handleKeyboardDidShow = event => {
    const keyboardHeight = event.endCoordinates.height;
    const currentlyFocusedInput = TextInputState.currentlyFocusedInput();

    if (currentlyFocusedInput) {
      UIManager.measure(currentlyFocusedInput, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = windowHeight - keyboardHeight - (fieldTop + fieldHeight);

        if (gap >= 0) return;

        Animated.timing(shift, {
          toValue: gap,
          easing: Easing.linear,
          duration: 200,
          useNativeDriver: true
        }).start();
      });
    }
  };

  const handleKeyboardDidHide = () => {
    Animated.timing(shift, {
      toValue: 0,
      easing: Easing.linear,
      duration: 200,
      useNativeDriver: true
    }).start();
  };

  return (
    <>
      {Platform.OS !== 'ios' && (
        <ScrollView
          keyboardDismissMode="on-drag"
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ ...styles.screenContainerStyle, ...props.contentContainerStyle }}
        >
          {props.children}
        </ScrollView>
      )}
      {Platform.OS === 'ios' && (
        <Animated.View style={[styles.container, { transform: [{ translateY: shift }] }]}>
          <ScrollView
            keyboardDismissMode="on-drag"
            keyboardShouldPersistTaps="handled"
            contentContainerStyle={{
              ...styles.screenContainerStyle,
              ...props.contentContainerStyle
            }}
          >
            {props.children}
          </ScrollView>
        </Animated.View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%'
  },
  screenContainerStyle: {
    flexGrow: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 30
  }
});

export { KeyboardAwareScrollView };
