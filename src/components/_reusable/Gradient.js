import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Gradient = ({colors, style, children, useAngle, angle, start, end}) => (
  <LinearGradient
    start={start || {x: 0, y: 0}}
    end={end || {x: 1, y: 0}}
    useAngle={useAngle}
    angle={angle}
    colors={colors || ['#E572A5', '#DD9163']}
    style={[styles.containerStyle, style]}>
    {children}
  </LinearGradient>
);

const styles = StyleSheet.create({
  containerStyle: {
    borderRadius: 0,
    padding: 0,
  },
});

export {Gradient};
