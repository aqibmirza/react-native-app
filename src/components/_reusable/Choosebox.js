import React from 'react';
import { StyleSheet, View } from 'react-native';
import Checkbox from '@react-native-community/checkbox';
import {colors} from '../../designSystem';
const ChooseBox = ({
  value,
  onValueChange,
  children,
  style,
  disabled = false,
}) => (

  <View style={{ ...styles.row, ...style }}>  
    <View style={styles.leftColumn}>
      <Checkbox
        onValueChange={onValueChange}
        value={value}
        style={{ color: colors.blue }}
        disabled={disabled}
        
      />
    </View>
    <View style={styles.rightColumn}>{children}</View>
  </View>
);

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
   // justifyContent: 'space-between',
    alignSelf: 'stretch'
  },
  leftColumn: { flexShrink: 1 ,
 },
  rightColumn: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
   flexShrink: 0,

  },
  lock: { marginHorizontal: 5 }
});

export { ChooseBox };
