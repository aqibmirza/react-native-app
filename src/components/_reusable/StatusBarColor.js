import { colors } from '#/designSystem';
import React from 'react';
import { View, StatusBar, StyleSheet, Platform} from 'react-native';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBar.currentHeight;

const StatusBarColor = ({ backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
    );

    

const styles = StyleSheet.create({

    statusBar: {
    height: STATUSBAR_HEIGHT
    }
});
export { StatusBarColor };