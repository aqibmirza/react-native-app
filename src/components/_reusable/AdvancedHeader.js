import { colors, PageTitle } from '#/designSystem';
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Touchable } from './Touchable';

const AdvancedHeader = ({ leftIcon, rightIcon, title, component, onPress, minHeight = 84,leftOnPress,rightOnPress }) => {
    const { buttonStyle, menuStyle, menuFirstSection } = styles;

    return (
        <>
            <View style={[menuStyle, { height: minHeight }]}>
                <View style={menuFirstSection}>

                  <Touchable style={{width:20}} onPressIn={leftOnPress}>
                        {leftIcon}
                    </Touchable>
                    <View>
                        <PageTitle fontWeight="700">{title}</PageTitle>
                    </View>
                        <Touchable style={{width:20}} onPressIn={rightOnPress}>
                            {rightIcon}
                        </Touchable>
                </View>
                {component}
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    menuStyle: {
        backgroundColor: colors.blue,
        // flexGrow:1,
        paddingHorizontal: 30,
        paddingVertical: 20,
        borderBottomRightRadius: 40,
        borderBottomLeftRadius: 40
    },
    menuFirstSection: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonStyle: {
        width: 46,
        height: 46,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export { AdvancedHeader };
