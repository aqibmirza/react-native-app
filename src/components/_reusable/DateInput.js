import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { InputLabel } from './InputLabel';
import { ErrorMessage } from './ErrorMessage';
import { P1, DateTime } from '../../designSystem';

const DateInput = props => {
  const { placeholder, value, onFocus, label, error } = props;
  const marginBottom = error ? 0 : 10;

  return (
    <View style={{ marginBottom }}>
      {label && <InputLabel label={label} />}
      <View style={styles.inputContainerStyle}>
        <TouchableOpacity onPress={onFocus}>
          {value && <DateTime date={value} wrapper={P1} opacity={1} />}
          {!value && placeholder && <P1>{placeholder}</P1>}
        </TouchableOpacity>
      </View>
      {error && <ErrorMessage description={error} />}
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainerStyle: {
    paddingHorizontal: 15,
    paddingVertical: 13,
    backgroundColor: '#373A40',
    borderRadius: 6
  }
});

export { DateInput };
