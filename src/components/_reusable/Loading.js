import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Loading = ({ transparent = false }) => (
  <View
    testID="loader"
    style={{ flex: 1, backgroundColor: transparent ? 'transparent' : '#fff' }}
  >
    <ActivityIndicator
      size="large"
      color="#F6222A"
      style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
    />
  </View>
);

export { Loading };
