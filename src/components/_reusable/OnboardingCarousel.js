import React from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import Carousel from 'react-native-snap-carousel';

import {colors, H2, P2} from '../../designSystem';

const {width} = Dimensions.get('window');

const carouselItems = [
  {
    title: 'headers.1',
    description: 'descriptions.1',
  },
  {
    title: 'headers.2',
    description: 'descriptions.2',
  },
  {
    title: 'headers.3',
    description: 'descriptions.3',
  },
  {
    title: 'headers.4',
    description: 'descriptions.4',
  },
  {
    title: 'headers.5',
    description: 'descriptions.5',
  },
];

const renderItem = ({item}) => (
  <View style={styles.containerStyle}>
    <H2 centered style={{marginTop: 20, marginBottom: 8}}>
      {item.title}
    </H2>
    <P2 centered opacity={0.6}>
      {item.description}
    </P2>
  </View>
);

const OnboardingCarousel = () => (
  <View>
    <Carousel
      data={carouselItems}
      keyExtractor={({title}) => title}
      renderItem={renderItem}
      itemWidth={width - 100}
      sliderWidth={width}
      onSnapToItem={() => {}}
      inactiveSlideScale={1}
      activeSlideAlignment="center"
    />
  </View>
);

const styles = StyleSheet.create({
  containerStyle: {
    alignItems: 'center',
    backgroundColor: colors.greyLight,
    marginVertical: 20,
    height: 320,
    paddingTop: 40,
    paddingHorizontal: 16,
    borderRadius: 6,
    marginHorizontal: 10,
  },
  titleContainer: {
    height: 65,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25,
  },
  titleStyle: {
    flexWrap: 'wrap',
    paddingHorizontal: 10,
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
  },
  descriptionStyle: {
    flexWrap: 'wrap',
    paddingHorizontal: 10,
    fontSize: 17,
    color: 'rgba(255, 255, 255, 0.6)',
    marginTop: 15,
    textAlign: 'center',
  },
});

export {OnboardingCarousel};
