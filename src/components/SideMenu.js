import React from 'react';
import {StyleSheet, ImageBackground, View, Dimensions} from 'react-native';
import {colors, P1, P2} from '../designSystem';
import {Touchable} from './_reusable';
import Logout from '../resources/img/svg/Logout';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderColor: 'red',
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
});
const {height, width} = Dimensions.get('window');
const MenuItem = ({icon, title, onPress}) => {
  return (
    <Touchable
      style={{flexDirection: 'row', alignItems: 'center', marginBottom: 18}}
      onPress={onPress}>
      {icon}
      <P2 color={colors.white} opacity={1} style={{marginLeft: 12}}>
        {title}
      </P2>
    </Touchable>
  );
};
const SideMenu = props => {
  return (
    <ImageBackground source={{uri: 'MenuBg'}} style={styles.image}>
      <View>
        <View style={{flexDirection: 'row', marginLeft: 15}}>
          <View style={{marginLeft: 18, justifyContent: 'center'}}>
            <P1 color={colors.white} opacity={1}>
              John Smith
            </P1>
            <P1 color={colors.white} opacity={1}>
              +91 9999999999
            </P1>
          </View>
        </View>
        <View style={{marginLeft: 22, marginTop: 44}}>
          <MenuItem
            icon={<Logout />}
            title="Home"
            onPress={() => alert('hello')}
          />
          <MenuItem
            icon={<Logout />}
            title="Profile"
            onPress={() => alert('hello')}
          />
          <MenuItem
            icon={<Logout />}
            title="Settings"
            onPress={() => alert('hello')}
          />
          <MenuItem
            icon={<Logout />}
            title="Log out"
            onPress={() => alert('hello')}
          />
        </View>
      </View>
    </ImageBackground>
  );
};

export default SideMenu;
