import { setInAppNotification, inAppNotifyNavigate } from '../actions/InAppNotificationActions';
import { Store } from '../Store';

export const inAppNotifyTrigger = async ({ data }, isForeground, canNavigate = true) => {
  const isInAppNotify = data['in-app'];

  const { customerId } = Store.getState().user;

  if (isInAppNotify) {
    await Store.dispatch(
      setInAppNotification({
        notification: data,
        isPremium: !!customerId
      })
    );

    if (canNavigate) await Store.dispatch(inAppNotifyNavigate(isForeground, data, !!customerId));
  }
};
