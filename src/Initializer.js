import React, {useState, useEffect} from 'react';
import {StyleSheet, Platform} from 'react-native';
import {connect} from 'react-redux';
// import messaging from '@react-native-firebase/messaging';
import DeviceInfo from 'react-native-device-info';
import NetInfo from '@react-native-community/netinfo';

import {Actions, ActionConst} from 'react-native-router-flux';
import {
  NeedUpdate,
  NetworkDisconnect,
  SplashScreen,
} from './components/_reusable';

const isCompatible = async () => {
  //return `true` by default to avoid false positives
  try {
    const platform = Platform.OS.toUpperCase();
    const version = DeviceInfo.getVersion();
    const buildNumber = DeviceInfo.getBuildNumber();
    // const response = await applicationService.isCompatible({ platform, version, buildNumber });
    // if (typeof response === 'boolean') {
    //   return response;
    // }
    return true;
  } catch (e) {
    return true;
  }
};

const Initializer = props => {
  const [loading, setLoading] = useState(true);
  const [needUpdate, setNeedUpdate] = useState(false);
  const [networkConnected, setNetworkConnected] = useState(true);

  const fetchNetInfo = async () => {
    const {isConnected} = await NetInfo.fetch();
    setNetworkConnected(isConnected);
  };

  useEffect(() => {
    fetchNetInfo();
  }, []);

  useEffect(() => {
    // eslint-disable-next-line no-undef
    // if (__DEV__) {
    //   messaging().onNotificationOpenedApp(remoteMessage => {
    //     console.log(
    //       'Notification caused app to open from background state:',
    //       remoteMessage.notification,
    //     );
    //     navigation.navigate(remoteMessage.data.type);
    //   });

    //   // Check whether an initial notification is available
    //   messaging()
    //     .getInitialNotification()
    //     .then(remoteMessage => {
    //       if (remoteMessage) {
    //         console.log(
    //           'Notification caused app to open from quit state:',
    //           remoteMessage.notification,
    //         );
    //         setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
    //       }
    //       setLoading(false);
    //     });
    // }

    const load = async () => {
      //Check if current app version is compatible
      //If not, send to store
      if (!(await isCompatible())) {
        setNeedUpdate(true);
        setLoading(false);
        return;
      }

      setLoading(false);
      setTimeout(() => {
        Actions.login({type: ActionConst.RESET});
      }, 2000);
    };

    if (networkConnected) load();
  }, [networkConnected]);

  if (!networkConnected) return <NetworkDisconnect onPress={fetchNetInfo} />;

  if (loading) return <SplashScreen />;

  if (needUpdate) return <NeedUpdate />;

  return <>{props.children}</>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  text: {
    color: 'grey',
    fontSize: 30,
    fontWeight: 'bold',
  },
});
export default connect(null, {})(Initializer);
