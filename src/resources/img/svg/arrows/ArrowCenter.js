import React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgArrowCenter = props => (
  <Svg width={70} height={8} viewBox="0 0 70 8" fill="none" {...props}>
    <Path d="M0 0h64.05L69 3l-4.95 3H0l4.95-3L0 0z" fill={props.color} fillOpacity={0.6} />
  </Svg>
);

export default SvgArrowCenter;
