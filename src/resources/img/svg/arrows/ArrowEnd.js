import React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgArrowEnd = props => (
  <Svg width={70} height={8} viewBox="0 0 70 8" fill="none" {...props}>
    <Path d="M0 0h67a3 3 0 1 1 0 6H0l4.917-3L0 0z" fill={props.color} fillOpacity={0.6} />
  </Svg>
);

export default SvgArrowEnd;
