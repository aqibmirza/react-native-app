import React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgArrowStart = props => (
  <Svg width={70} height={8} viewBox="0 0 70 8" fill="none" {...props}>
    <Path
      d="M0 3a3 3 0 0 1 3-3h61.05L69 3l-4.95 3H3a3 3 0 0 1-3-3z"
      fill={props.color}
      fillOpacity={0.6}
    />
  </Svg>
);

export default SvgArrowStart;
